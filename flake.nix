{
  description = "EpiMac Docs";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    futils.url = "github:numtide/flake-utils";
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs = {
        nixpkgs-stable.follows = "nixpkgs";
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "futils";
      };
    };
  };

  outputs = { self, nixpkgs, futils, pre-commit-hooks } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (futils.lib) eachDefaultSystem;

      pkgImport = pkgs: system:
        import pkgs {
          inherit system;
          config.allowUnfree = true;
        };
    in
    eachDefaultSystem (system:
      let
        pkgs = pkgImport nixpkgs system;
        hook = pre-commit-hooks.lib.${system};
        tools = import "${pre-commit-hooks}/nix/call-tools.nix" pkgs;
      in
      rec {
        checks.pre-commit-check = hook.run {
          src = self;

          tools = tools;

          hooks = {
            nixpkgs-fmt.enable = true;
            eslint.enable = true;
          };
        };

        devShell = pkgs.mkShell {
          name = "EpiMac Docs";

          shellHook = ''
            ${checks.pre-commit-check.shellHook}
          '';

          buildInputs = with pkgs; [
            git
            nodejs_20
            nodePackages.pnpm
          ];

          packages = with pkgs; [
            tools.nixpkgs-fmt
            tools.eslint
          ];
        };
      }
    );
}
